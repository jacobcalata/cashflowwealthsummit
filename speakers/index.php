<?php
  // if($_SERVER["HTTPS"] != "on"){
  //   header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
  // }
  // session_start();
  //
  // header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  // header("Cache-Control: post-check=0, pre-check=0", false);
  // header("Pragma: no-cache");

  require_once('../src/functions/core.php');
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once('../src/components/head.php'); ?>
<title>Speakers: Cash Flow Wealth Summit</title>
<meta name="description" content="Cash Flow Wealth Summit is a financial virtual summit. Sign up to learn from industry leading experts.">
<link rel="canonical" href="http://cashflowwealthsummit.com/" />
<meta name="keywords" content="">
<meta property="og:url" content="http://cashflowwealthsummit.com/">
<meta property="og:title" content="Cash Flow Wealth Summit: Financial Virtual Summit 2016">
<meta property="og:description" content="Cash Flow Wealth Summit is a financial virtual summit. Sign up to learn from industry leading experts.">
<meta property="og:type" content="article">
<meta property="og:image" content="" data-pin-nopin="true">
</head>
<body>
<?php require_once('../src/components/header.php'); ?>
<main>
  <section class="speakers-heading-section heading-section content-section">
    <div class="heading-section-background"></div>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h1>Summit Speakers</h1>
        </div>
      </div>
    </div>
  </section>
  <section class="speakers-speakers-section content-section">
    <div class="container">
      <div class="content-section-title row">
        <div class="col-xs-12">
          <h2 class="uppercase-text">Keynote Speakers</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
      </div>
      <?php
        $speakers = getSpeakers(6);
        $speakerSize = count($speakers);

        for($i = 0; $i < $speakerSize; $i++) {
          if($i % 3 == 0){
      ?>
            <div class="row">
      <?php
        }
      ?>
        <div class="col-sm-4">
          <div class="speaker-bio">
            <a href="//cashflowwealthsummit.com/speakers/<?php echo $speakers[$i]['url_ref']; ?>/">
              <img class="speaker-bio-image" src="<?php echo $speakers[$i]['image_link']; ?>" alt="<?php echo $speakers[$i]['name']; ?>" />
            </a>
            <p class="speaker-bio-name green-text"><?php echo $speakers[$i]['name']; ?></p>
            <p class="speaker-bio-title"><?php echo $speakers[$i]['position']; ?></p>
            <p class="speaker-bio-company"><?php echo $speakers[$i]['company']; ?></p>
          </div>
        </div>
      <?php
        if($i % 3 == 2){
      ?>
          </div>
      <?php
          }
        }
      ?>
    </div>
  </section>
  <section class="speakers-day-section content-section">
    <div class="container">
      <div class="content-section-title row">
        <div class="col-xs-12">
          <h2 class="uppercase-text">Expert Speakers</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
      </div>
      <?php
        $speakers = getSpeakers(6);
        $speakerSize = count($speakers);

        for($i = 0; $i < $speakerSize; $i++) {
          if($i % 3 == 0){
      ?>
            <div class="row">
      <?php
        }
      ?>
        <div class="col-sm-4">
          <div class="speaker-bio">
            <a href="//cashflowwealthsummit.com/speakers/<?php echo $speakers[$i]['url_ref']; ?>/">
              <img class="speaker-bio-image" src="<?php echo $speakers[$i]['image_link']; ?>" alt="<?php echo $speakers[$i]['name']; ?>" />
            </a>
            <p class="speaker-bio-name green-text"><?php echo $speakers[$i]['name']; ?></p>
            <p class="speaker-bio-title"><?php echo $speakers[$i]['position']; ?></p>
            <p class="speaker-bio-company"><?php echo $speakers[$i]['company']; ?></p>
          </div>
        </div>
      <?php
        if($i % 3 == 2){
      ?>
          </div>
      <?php
          }
        }
      ?>
    </div>
  </section>
</main>
<?php require_once('../src/components/footer.php'); ?>
<?php require_once('../src/components/foot.php'); ?>
</body>
</html>
