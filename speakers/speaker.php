<?php
  // if($_SERVER["HTTPS"] != "on"){
  //     header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
  // }
  // session_start();
  require_once('../src/functions/core.php');
  $speaker = getSpeaker('patrick-donohoe');
?>
<!DOCTYPE html>
<html>
<head>
<title><?php echo $speaker[0]['name']; ?>: Cash Flow Wealth Summit</title>
<meta name="description" content="<?php echo $speaker[0]['name']; ?> of the Cash Flow Wealth Summit. The Cash Flow Wealth Summit is a free financial virtual summit. Please join us on May 20th-21st, 2015.">
<link rel="canonical" href="http://cashflowwealthsummit.com/speakers/<?php echo $speaker[0]['url_ref']; ?>/" />
<?php require_once('../src/components/head.php'); ?>
</head>
<body>
<?php require_once('../src/components/header.php'); ?>
<main>
  <section class="speakers-heading-section heading-section content-section">
    <div class="heading-section-background"></div>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h1><?php echo $speaker[0]['name']; ?></h1>
        </div>
      </div>
    </div>
  </section>
  <section class="single-speaker-section content-section">
    <div class="container">
      <!-- <div class="content-section-title row">
        <div class="col-xs-12">
          <h2 class="uppercase-text"><?php // echo $speaker[0]['position']; ?></h2>
          <p class="green-text"><?php // echo $speaker[0]['company']; ?></p>
        </div>
      </div> -->
      <div class="row">
        <div class="col-sm-4">
          <div class="speaker-bio">
            <img class="speaker-bio-image" src="<?php echo $speaker[0]['image_link']; ?>" alt="<?php echo $speaker[0]['name']; ?>" />
            <p class="speaker-bio-name green-text"><?php echo $speaker[0]['position']; ?></p>
            <p class="speaker-bio-title"><?php echo $speaker[0]['company']; ?></p>
          </div>
        </div>
        <div class="col-sm-8">
          <p><?php echo $speaker[0]['description']; ?></p>
        </div>
      </div>
    </div>
  </section>
</main>
<?php require_once('../src/components/footer.php'); ?>
<?php require_once('../src/components/foot.php'); ?>
</body>
</html>
