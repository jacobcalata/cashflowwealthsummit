jQuery(document).ready(function($) {
  $('.popup').click(function() {
    var screenWidth = $('body').width();
    var newWidth = (screenWidth - 600)/2;
    var NWin = window.open($(this).prop('href'), '', 'scrollbars=1,left=' + newWidth + ',top=150,height=425,width=600');
    if(window.focus) {
      NWin.focus();
    }
    return false;
  });
});
