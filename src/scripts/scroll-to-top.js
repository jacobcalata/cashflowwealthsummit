jQuery(document).ready(function($) {
  var scrollTopTime = 700,
  scrollTop = $('.scroll-top');
  scrollTop.on('click', function(event) {
    event.preventDefault();
    $('body,html').animate({
      scrollTop: 0,
    }, scrollTopTime);
  });
});
