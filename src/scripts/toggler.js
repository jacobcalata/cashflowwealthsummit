$(document).ready(function(e) {
	$('.comments-container > .comments:gt(0)').hide();
	setInterval(function() {
	  $('.comments-container > .comments:first')
	    .slideToggle(2000)
	    .next()
	    .slideToggle(2000)
	    .end()
	    .appendTo('.comments-container');
	}, 6000);
});
