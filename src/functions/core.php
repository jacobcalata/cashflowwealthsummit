<?php
	$mysql = new mysqli("localhost", "root", "root", "cashflow_summit");
	function getSpeakers($limit = 100, $order = null) {
		global $mysql;
		if($order == 'random') {
			$sql = "SELECT * FROM speakers WHERE image_link <> '' ORDER BY RAND() LIMIT " . $limit;
		} else {
			$sql = "SELECT * FROM speakers WHERE image_link <> '' ORDER BY speaker_order ASC";
		}
		$result = $mysql->query($sql);
		$speakers = array();
		while($row = $result->fetch_assoc()) {
			array_push($speakers, $row);
		}
		return $speakers;
		$mysql->close();
	}
	function getSpeaker($name) {
		global $mysql;
		$sql = "SELECT * FROM speakers WHERE url_ref = '" . $name . "'";
		$result = $mysql->query($sql);
		$speaker = array();
		while($row = $result->fetch_assoc()) {
			array_push($speaker, $row);
		}
		return $speaker;
		$mysql->close();
	}
	function getSpeakerById($id) {
		global $mysql;
		$sql = "SELECT * FROM speakers WHERE id = '" . $id . "'";
		$result = $mysql->query($sql);
		$speaker = array();
		while($row = $result->fetch_assoc()) {
			array_push($speaker, $row);
		}
		return $speaker;
		$mysql->close();
	}
?>
