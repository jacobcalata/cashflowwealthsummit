<script type="text/javascript" src="//platform.linkedin.com/in.js">
  api_key:   75xsrr6ey15737
  // authorize: true
  // onLoad: onLinkedInLoad
</script>
<script type="text/javascript">
  // Setup an event listener to make an API call once auth is complete
  function onLinkedInLoad() {
    IN.UI.Authorize().place();
    IN.Event.on(IN, "auth", getProfileData);
    console.log('loaded');
  }
  // Handle the successful return from the API call
  function onSuccess(data) {
  // var linkedinData = $.map(data, function(value, index) {
  //     return [value];
  // });
  var hasAccess = getCookie('userHasAccess');
      if(hasAccess == ""){
    MktoForms2.whenReady(function (form){
      form.vals({"FirstName":data.values[0].firstName, "LastName":data.values[0].lastName, "Email":data.values[0].emailAddress, "Phone":"(123)123-1233"});
      form.onValidate(function(){
        var vals = form.vals();
        var phoneElem = form.getFormElem().find("#Phone");
        var emailElem = form.getFormElem().find("#Email");
        if(vals.Phone == ''){
          form.submittable(false);
          form.showErrorMessage("Please enter a valid phone number.", phoneElem);
        } else if(vals.Email == ''){
          form.submittable(false);
          form.showErrorMessage("Please enter a valid email address.", emailElem);

        } else if (vals.Phone == '' && vals.Email == ''){
          form.submittable(false);
          form.showErrorMessage("Please enter a valid phone number.", phoneElem);
          form.showErrorMessage("Please enter a valid email address.", emailElem);
        } else {
          form.submittable(true);
        }
      })
      ga('send', 'event', 'form', 'submit', 'CFWSummit-Linkedin');
      form.submit();
  });
  }


  }

  // Handle an error response from the API call
  function onError(error) {
      console.log(error);
  }

  // Use the API call wrapper to request the member's basic profile data
  function getProfileData() {
      IN.API.Profile('me').fields('firstName', 'lastName', 'email-address').result(onSuccess).error(onError);
  }

  function logUserOut() {
    IN.User.logout(callbackFunction, callbackScope);
  }

</script>
<script>
function fb_login() {
  FB.login(function(response) {
    if (response.status === 'connected') {
      var hasAccess = getCookie('userHasAccess');
      if(hasAccess == ''){
        fillmktoForm();
      }
    }
  }, { scope: 'email,public_profile' } );
}
function statusChangeCallback(response) {
  if (response.status === 'connected') {
    var hasAccess = getCookie('userHasAccess');
    if(hasAccess == ''){
      fillmktoForm();
    }
  }
}
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    fb_login(response);
  });
}
window.fbAsyncInit = function() {
  FB.init({
    appId      : '441940799312622',
    xfbml      : true,
    version    : 'v2.3'
  });
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
};
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
function fillmktoForm() {
  FB.api('/me', function(response) {
  MktoForms2.whenReady(function (form){
    form.vals({"FirstName":response.first_name, "LastName":response.last_name, "Email":response.email, "Phone":"(123)123-1233"});
    form.onValidate(function(){
      var vals = form.vals();
      var phoneElem = form.getFormElem().find("#Phone");
      var emailElem = form.getFormElem().find("#Email");
      if(vals.Phone == ''){
        form.submittable(false);
        form.showErrorMessage("Please enter a valid phone number.", phoneElem);
      } else if(vals.Email == ''){
        form.submittable(false);
        form.showErrorMessage("Please enter a valid email address.", emailElem);

      } else if (vals.Phone == '' && vals.Email == ''){
        form.submittable(false);
        form.showErrorMessage("Please enter a valid phone number.", phoneElem);
        form.showErrorMessage("Please enter a valid email address.", emailElem);
      } else {
        form.submittable(true);
      }
    })
    ga('send', 'event', 'form', 'submit', 'CFWSummit-Facebook');
    form.submit();
   });
  });
}
</script>
<script>
  $(document).ready(function() {
    $('#registration-form').modal();
  });
</script>
<!-- <script src="/dev/scripts/main.js" type="text/javascript" charset="utf-8"></script> -->
<script src="/dist/scripts/main.min.js" type="text/javascript" charset="utf-8"></script>
