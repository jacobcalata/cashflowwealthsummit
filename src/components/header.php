<header>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="mobile-nav-icon">
          <i class="mobile-nav-icon-toggle fa fa-bars" aria-hidden="true"></i>
        </div>
        <nav>
          <ul>
            <li class="home-link">
              <a class="uppercase-text" href="/">Home</a>
            </li>
            <li>
              <a class="padding-fix uppercase-text" href="/speakers">Speakers</a>
            </li>
            <li>
              <a class="uppercase-text" href="/schedule">Schedule</a>
            </li>
            <li class="logo-link">
              <a href="/"><img src="/src/images/cash-flow-wealth-summit-logo.png" alt="Cash Flow Wealth Summit Logo" /></a>
            </li>
            <li>
              <a class="uppercase-text" href="/">2015 Archive</a>
            </li>
            <li>
              <a class="small-button fancy-button uppercase-text" href="/register">Register</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>
