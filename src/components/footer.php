<footer>
  <div class="scroll-top">
    <i class="fa fa-angle-up" aria-hidden="true"></i>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="legal-box">
          <ul>
            <li><a class="uppercase-text" href="/privacy-policy.php">Privacy</a></li>
            <li>|</li>
            <li><a class="uppercase-text" href="/disclaimer.php">Disclaimer</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="socials-box">
          <ul>
            <li>
              <a href="https://www.facebook.com/CashFlowWealthSummit" target="_blank"><i class="fa fa-facebook"></i></a>
            </li>
            <li>
              <a href="https://twitter.com/cfwsummit" target="_blank"><i class="fa fa-twitter"></i></a>
            </li>
            <li>
              <a href="https://www.linkedin.com/company/cash-flow-wealth-summit" target="_blank"><i class="fa fa-linkedin"></i></a>
            </li>
            <li>
              <a href="https://plus.google.com/107597441664332259603/about" target="_blank"><i class="fa fa-google" aria-hidden="true"></i></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
