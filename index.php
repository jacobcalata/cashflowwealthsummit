<?php
  // if($_SERVER["HTTPS"] != "on"){
  //   header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
  // }
  // session_start();
  //
  // header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  // header("Cache-Control: post-check=0, pre-check=0", false);
  // header("Pragma: no-cache");

  require_once('./src/functions/core.php');
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once('./src/components/head.php'); ?>
<title>Cash Flow Wealth Summit: Financial Virtual Summit 2016</title>
<meta name="description" content="Cash Flow Wealth Summit is a financial virtual summit. Sign up to learn from industry leading experts.">
<link rel="canonical" href="http://cashflowwealthsummit.com/" />
<meta name="keywords" content="">
<meta property="og:url" content="http://cashflowwealthsummit.com/">
<meta property="og:title" content="Cash Flow Wealth Summit: Financial Virtual Summit 2016">
<meta property="og:description" content="Cash Flow Wealth Summit is a financial virtual summit. Sign up to learn from industry leading experts.">
<meta property="og:type" content="article">
<meta property="og:image" content="" data-pin-nopin="true">
</head>
<body>
<?php require_once('./src/components/header.php'); ?>
<main>
  <section class="home-heading-section heading-section content-section">
    <div class="heading-section-background"></div>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h1>June 8 &amp; 9, 2016</h1>
          <h2>A Virtual Financial Summit</h2>
          <p>Identify opportunities in your life to build your wealth, cash flow, and financial properity. Discover new ways to boost cash flow by experts of investing, real estate, tax strategy, estate planning, marketing, oil &amp; gas, and more. Join us in June for the world’s premier virtual financial summit. Register early and attend our two day event for <span>FREE</span>.</p>
          <div class="sign-up-form-box">
            <script src="//app-ab02.marketo.com/js/forms2/js/forms2.min.js"></script><form id="mktoForm_1340"></form><script>MktoForms2.loadForm("//app-ab02.marketo.com", "439-DCI-591", 1340, function(form){form.onSuccess(function(values, followUpUrl){var vals = form.vals();setCookie("userHasAccess",vals.FirstName,365);location.href="//cashflowwealthsummit.com/thankyou.php"; return false;})});</script>
            <p>*Enter email address to begin your free registration</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="home-speakers-section content-section">
    <div class="container">
      <div class="content-section-title row">
        <div class="col-xs-12">
          <h2 class="uppercase-text">Keynote Speakers</h2>
          <p>Cash flow secrets &amp; investing tips from over 30 brilliant financial minds.</p>
        </div>
      </div>
      <?php
        $speakers = getSpeakers(6);
        $speakerSize = count($speakers);

        for($i = 0; $i < $speakerSize; $i++) {
          if($i % 3 == 0){
      ?>
            <div class="row">
      <?php
        }
      ?>
        <div class="col-sm-4">
          <div class="speaker-bio">
            <a href="//cashflowwealthsummit.com/speakers/<?php echo $speakers[$i]['url_ref']; ?>/">
              <img class="speaker-bio-image" src="<?php echo $speakers[$i]['image_link']; ?>" alt="<?php echo $speakers[$i]['name']; ?>" />
            </a>
            <p class="speaker-bio-name green-text"><?php echo $speakers[$i]['name']; ?></p>
            <p class="speaker-bio-title"><?php echo $speakers[$i]['position']; ?></p>
            <p class="speaker-bio-company"><?php echo $speakers[$i]['company']; ?></p>
          </div>
        </div>
      <?php
        if($i % 3 == 2){
      ?>
          </div>
      <?php
          }
        }
      ?>
    </div>
  </section>
  <section class="home-schedule-section content-section">
    <div class="container">
      <div class="content-section-title row">
        <div class="col-md-offset-2 col-md-8">
          <h2 class="uppercase-text">Day 1</h2>
          <p class="schedule-sponsor-title uppercase-text green-text">Sponsored by Rich Dad</p>
          <p class="schedule-date">Wednesday, June 8th, 2016</p>
          <div class="row">
            <div class="col-sm-6">
              <p class="option-title green-text">Speaker One</p>
              <ul>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
              </ul>
            </div>
            <div class="col-sm-6">
              <p class="option-title green-text">Speaker Two</p>
              <ul>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
              </ul>
            </div>
          </div>
          <a class="big-button fancy-button uppercase-text" href="/register">Register Here</a>
        </div>
      </div>
      <div class="content-section-title row">
        <div class="col-md-offset-2 col-md-8">
          <h2 class="uppercase-text">Day 2</h2>
          <p class="schedule-sponsor-title uppercase-text green-text">Sponsored by Rich Dad</p>
          <p class="schedule-date">Wednesday, June 8th, 2016</p>
          <div class="row">
            <div class="col-sm-6">
              <p class="option-title green-text">Speaker One</p>
              <ul>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
              </ul>
            </div>
            <div class="col-sm-6">
              <p class="option-title green-text">Speaker Two</p>
              <ul>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
                <li>8:30 AM - 9:30 AM LOREM IPSUM</li>
                <li>10:30 AM - 1:30 PM LOREM IPSUM</li>
              </ul>
            </div>
          </div>
          <a class="big-button fancy-button uppercase-text" href="/register">Register Here</a>
        </div>
      </div>
    </div>
  </section>
</main>
<?php require_once('./src/components/footer.php'); ?>
<?php require_once('./src/components/foot.php'); ?>
</body>
</html>
