<!DOCTYPE html>
<html>
<head>
<?php require_once('../src/components/head.php'); ?>
<title>Schedule: Cash Flow Wealth Summit</title>
<meta name="description" content="Cash Flow Wealth Summit is a financial virtual summit. Sign up to learn from industry leading experts.">
<link rel="canonical" href="http://cashflowwealthsummit.com/" />
<meta name="keywords" content="">
<meta property="og:url" content="http://cashflowwealthsummit.com/">
<meta property="og:title" content="Cash Flow Wealth Summit: Financial Virtual Summit 2016">
<meta property="og:description" content="Cash Flow Wealth Summit is a financial virtual summit. Sign up to learn from industry leading experts.">
<meta property="og:type" content="article">
<meta property="og:image" content="" data-pin-nopin="true">
</head>
<body>
<?php require_once('../src/components/header.php'); ?>
<main>
  <section class="schedule-heading-section heading-section content-section">
    <div class="heading-section-background"></div>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h1>Summit Registration</h1>
        </div>
      </div>
    </div>
  </section>
  <section class="register-section content-section">
    <div class="container">
      <div class="row">
        <div class="content-section-title row">
          <div class="col-xs-12">
            <h2 class="uppercase-text">Register Free</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-offset-4 col-sm-4">
          <div class="register-form-box">
            <script>
              // Social login markup
              // Do we need this?
              // <div class="social-login-block"><div class="social-login-container"><div class="facebook social-login"><div><a class="btn" onclick="fb_login()">Facebook</a></div></div><div class="linkedin social-login"><div><a class="btn" onclick="onLinkedInLoad()">Linkedin</a></div></div></div></div>
              function linkClicked() {
                ga('send', 'event', 'form', 'submit', 'CFWSummit');
              };
              if(isLoggedIn) {
                document.write('<p class="thank-you">Thank You For Registering<br>You have access to all videos using the menu above.</p>');
              } else {
                document.write('<script src="//app-ab02.marketo.com/js/forms2/js/forms2.min.js"><\/script><form id="mktoForm_1249"></form><script>MktoForms2.loadForm("//app-ab02.marketo.com", "439-DCI-591", 1249, function(form){form.onSuccess(function(values, followUpUrl){var vals = form.vals();setCookie("userHasAccess",vals.FirstName,365);linkClicked();location.href="//cashflowwealthsummit.com/thankyou.php"; return false;})});<\/script>');
              };
            </script>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
<?php require_once('../src/components/footer.php'); ?>
<?php require_once('../src/components/foot.php'); ?>
</body>
</html>
