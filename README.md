# Cash Flow Wealth Summit

## Environment
> Some kind of PHP processing and MySQL database like the Vagrant Scotch box

* Vagrant
* PHP
* Node
* NPM
* Gulp

## Usage
> You can ignore the package.json, gulpfile.js, bower.json, .bowerrc if you are not using them in the build process. Other than that they will just come in handy if you are

1. Install dependencies ```npm install``` and ```bower install```
2. Run default gulp task to compile files ```gulp```
3. Run serve gulp task to watch for file changes ```gulp serve```
4. Run build gulp task to build files for production```gulp build```

## Useful
* [Vagrant](https://www.vagrantup.com/docs/getting-started/)
* [Scotch](https://box.scotch.io/)
* [NPM](https://docs.npmjs.com/)
* [Gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
* [Bower](http://bower.io/#getting-started)
* [Bootstrap](http://getbootstrap.com/css/)
* [jQuery](http://api.jquery.com/)
