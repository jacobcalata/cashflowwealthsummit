'use strict';

// Load plugins
var gulp = require('gulp'),
del = require('del'),
$ = require('gulp-load-plugins')();

gulp.task('scripts:dev', function() {
	return gulp.src([
		'./src/vendor/jquery/dist/jquery.min.js',
		'./src/vendor/bootstrap-sass/assets/javascripts/bootstrap.min.js',
		'./src/scripts/**/*.js'
	])
		.pipe($.sourcemaps.init())
			.pipe($.concat('main.js'))
		.pipe($.sourcemaps.write())
		.pipe(gulp.dest('./dev/scripts'));
});

gulp.task('scripts:dist', ['scripts:dev'], function() {
	return gulp.src('./dev/scripts/*.js')
		.pipe($.uglify())
		.pipe($.rename('main.min.js'))
		.pipe(gulp.dest('./dist/scripts'));
});

gulp.task('styles:dev', function() {
	return gulp.src('./src/styles/*.scss')
		.pipe($.sourcemaps.init())
			.pipe($.sass())
		.pipe($.sourcemaps.write())
		.pipe(gulp.dest('./dev/styles'));
});

gulp.task('styles:dist', ['styles:dev'], function() {
	return gulp.src('./dev/styles/*.css')
		.pipe($.cssnano())
		.pipe($.rename('main.min.css'))
		.pipe(gulp.dest('./dist/styles'));
});

gulp.task('watch', function() {
	gulp.watch('./src/styles/**/*.scss', ['styles:dev']);
	gulp.watch('./src/scripts/**/*.js', ['scripts:dev']);
})

gulp.task('clean', function() {
	del([
		'./dev',
		'./node_modules',
		'./src/vendor'
	]);
});

gulp.task('default', ['styles:dev', 'scripts:dev']);
gulp.task('serve', ['default', 'watch']);
gulp.task('build', ['styles:dist', 'scripts:dist']);
